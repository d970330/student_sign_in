/*
 Navicat Premium Data Transfer

 Source Server         : dbs
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : sa

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 12/06/2020 09:43:48
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for student
-- ----------------------------
DROP TABLE IF EXISTS `student`;
CREATE TABLE `student`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `sid` int(0) NOT NULL,
  `student_name` char(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `times` tinyint(0) NOT NULL DEFAULT 0,
  `lecture` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `sid`(`sid`) USING BTREE,
  UNIQUE INDEX `name`(`student_name`) USING BTREE,
  INDEX `lecture`(`lecture`) USING BTREE,
  CONSTRAINT `lecture` FOREIGN KEY (`lecture`) REFERENCES `lecture` (`lecture_id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of student
-- ----------------------------
INSERT INTO `student` VALUES (1, 10000, 'lim', 0, 'COMP3910');
INSERT INTO `student` VALUES (2, 11111, 'qin', 0, 'COMP3910');
INSERT INTO `student` VALUES (3, 33333, 'qq', 0, 'COMP3910');
INSERT INTO `student` VALUES (11, 66666, 'qiuu', 0, 'COMP3910');
INSERT INTO `student` VALUES (12, 8888888, 'linqiaochu', 0, 'COMP3910');
INSERT INTO `student` VALUES (13, 87444, 'liqin', 0, 'COMP3910');
INSERT INTO `student` VALUES (15, 666666, 'qiaoccc', 0, 'COMP3910');
INSERT INTO `student` VALUES (18, 666122, 'qwwiaoccc', 0, 'COMP3910');
INSERT INTO `student` VALUES (19, 666222, 'qssiaoccc', 0, 'COMP3999');
INSERT INTO `student` VALUES (20, 78945, 'song', 0, 'COMP3910');
INSERT INTO `student` VALUES (21, 123456, 'handsomeboy', 0, 'COMP3910');
INSERT INTO `student` VALUES (22, 6666676, 'ququq', 0, 'COMP3910');
INSERT INTO `student` VALUES (23, 88899, 'qiaochu', 0, 'COMP3910');

SET FOREIGN_KEY_CHECKS = 1;
