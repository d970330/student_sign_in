/*
 Navicat Premium Data Transfer

 Source Server         : dbs
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : sa

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 12/06/2020 09:43:42
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for register
-- ----------------------------
DROP TABLE IF EXISTS `register`;
CREATE TABLE `register`  (
  `register_id` int(0) NOT NULL AUTO_INCREMENT,
  `lecture_id` int(0) NULL DEFAULT NULL,
  `lecture_num` int(0) NOT NULL DEFAULT 1,
  `student_id` int(0) NULL DEFAULT NULL,
  `register_time` datetime(0) NOT NULL ON UPDATE CURRENT_TIMESTAMP(0),
  `If_registed` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`register_id`) USING BTREE,
  INDEX `lecture_id`(`lecture_id`) USING BTREE,
  INDEX `student_id`(`student_id`) USING BTREE,
  CONSTRAINT `lecture_id` FOREIGN KEY (`lecture_id`) REFERENCES `lecture` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  CONSTRAINT `student_id` FOREIGN KEY (`student_id`) REFERENCES `student` (`id`) ON DELETE SET NULL ON UPDATE SET NULL
) ENGINE = InnoDB AUTO_INCREMENT = 208 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of register
-- ----------------------------
INSERT INTO `register` VALUES (3, 2, 1, 1, '2020-06-12 00:20:25', 0);
INSERT INTO `register` VALUES (4, 2, 1, 1, '2020-06-12 00:20:25', 0);
INSERT INTO `register` VALUES (5, 2, 2, 1, '2020-06-12 00:19:30', 1);
INSERT INTO `register` VALUES (6, 2, 1, 1, '2020-06-12 00:20:25', 0);
INSERT INTO `register` VALUES (16, 1, 4, 1, '2020-06-12 02:01:38', 0);
INSERT INTO `register` VALUES (17, 1, 4, 2, '2020-06-12 02:01:38', 0);
INSERT INTO `register` VALUES (18, 1, 4, 3, '2020-06-12 02:01:38', 0);
INSERT INTO `register` VALUES (19, 1, 4, 11, '2020-06-12 02:01:38', 0);
INSERT INTO `register` VALUES (20, 1, 4, 12, '2020-06-12 02:01:38', 0);
INSERT INTO `register` VALUES (21, 1, 4, 13, '2020-06-12 02:01:38', 0);
INSERT INTO `register` VALUES (22, 1, 4, 15, '2020-06-12 02:01:38', 0);
INSERT INTO `register` VALUES (23, 1, 4, 18, '2020-06-12 02:01:38', 0);
INSERT INTO `register` VALUES (24, 1, 6, 1, '2020-06-12 02:47:23', 0);
INSERT INTO `register` VALUES (25, 1, 6, 2, '2020-06-12 02:47:23', 0);
INSERT INTO `register` VALUES (26, 1, 6, 3, '2020-06-12 02:47:23', 0);
INSERT INTO `register` VALUES (27, 1, 6, 11, '2020-06-12 02:47:23', 0);
INSERT INTO `register` VALUES (28, 1, 6, 12, '2020-06-12 02:47:23', 0);
INSERT INTO `register` VALUES (29, 1, 6, 13, '2020-06-12 02:47:23', 0);
INSERT INTO `register` VALUES (30, 1, 6, 15, '2020-06-12 02:47:23', 0);
INSERT INTO `register` VALUES (31, 1, 6, 18, '2020-06-12 02:47:23', 0);
INSERT INTO `register` VALUES (32, 1, 7, 1, '2020-06-12 02:48:35', 0);
INSERT INTO `register` VALUES (33, 1, 7, 2, '2020-06-12 02:48:35', 0);
INSERT INTO `register` VALUES (34, 1, 7, 3, '2020-06-12 02:48:35', 0);
INSERT INTO `register` VALUES (208, 1, 1, 1, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (209, 1, 1, 2, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (210, 1, 1, 3, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (211, 1, 1, 11, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (212, 1, 1, 12, '2020-06-12 07:36:20', 1);
INSERT INTO `register` VALUES (213, 1, 1, 13, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (214, 1, 1, 15, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (215, 1, 1, 18, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (216, 1, 1, 20, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (217, 1, 1, 21, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (218, 1, 1, 22, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (219, 1, 1, 23, '2020-06-12 07:36:08', 0);
INSERT INTO `register` VALUES (220, 1, 2, 1, '2020-06-12 07:38:36', 0);
INSERT INTO `register` VALUES (221, 1, 2, 2, '2020-06-12 07:38:36', 0);
INSERT INTO `register` VALUES (222, 1, 2, 3, '2020-06-12 07:38:36', 0);
INSERT INTO `register` VALUES (223, 1, 2, 11, '2020-06-12 07:38:36', 0);
INSERT INTO `register` VALUES (224, 1, 2, 12, '2020-06-12 07:38:52', 1);
INSERT INTO `register` VALUES (225, 1, 2, 13, '2020-06-12 07:38:36', 0);
INSERT INTO `register` VALUES (226, 1, 2, 15, '2020-06-12 07:38:36', 0);
INSERT INTO `register` VALUES (227, 1, 2, 18, '2020-06-12 07:38:36', 0);
INSERT INTO `register` VALUES (228, 1, 2, 20, '2020-06-12 07:38:36', 0);
INSERT INTO `register` VALUES (229, 1, 2, 21, '2020-06-12 07:38:36', 0);
INSERT INTO `register` VALUES (230, 1, 2, 22, '2020-06-12 07:38:36', 0);
INSERT INTO `register` VALUES (231, 1, 2, 23, '2020-06-12 07:38:36', 0);

SET FOREIGN_KEY_CHECKS = 1;
