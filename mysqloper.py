from datetime import *

import pymysql
import re

class Mysqloper():
    def __init__(self, parent=None):

        # super(mysqloper, self).__init__(parent)
        self.db = pymysql.connect(host="127.0.0.1", port=3306, user="root", passwd="123qwe", db="sa")
        # 得到一个可以执行SQL语句的光标对象
        self.cursor = self.db.cursor()  # 使用 execute() 方法执行 SQL 查询
        self.cursor.execute('SELECT VERSION()')
        self.data = self.cursor.fetchone()
        print("Database version : %s " % self.data)
        # 关闭光标对象
        self.cursor.close()

    # 链接数据库
    def mysqlconnect(host="127.0.0.1", port=3306, user="root", passwd="123qwe", db="sa"):
        db = pymysql.connect(host=host, port=port, user=user, passwd=passwd, db=db)
        return db

    # 执行语句
    def executeCreate(self,con, sql):
        cursor = con.cursor()  # 使用 execute() 方法执行 SQL 查询
        ret = cursor.execute(sql)
        cursor.close()
        return ret

    # 执行插入和更新
    def executeInsertAndUpdate(self,con, sql, listParams):
        #try:
            cursor = con.cursor()  # 使用 execute() 方法执行 SQL 查询
            ret = cursor.execute(sql, listParams)
            # 提交事务
            self.db.commit()
            cursor.close()
            return ret
        #except:
         #   print("跳出")
         #   return 0


    def table_exists(self,con, table_name):  # 这个函数用来判断表是否存在
        sql = "show tables;"
        cursor = con.cursor()
        cursor.execute(sql)
        tables = [cursor.fetchall()]
        table_list = re.findall('(\'.*?\')', str(tables))
        table_list = [re.sub("'", '', each) for each in table_list]
        cursor.close()
        if table_name in table_list:
            return 1  # 存在返回1
        else:
            return 0  # 不存在返回0

    # 插入函数
    def Insert_student(self,table, sid, name, lecture):
        times = 0
        sql = "INSERT INTO {0}(sid,student_name,lecture) VALUES (%s,%s,%s);"
        # 执行SQL语句
        #print(sql.format(table,sid,name,lecture))
        ret = self.executeInsertAndUpdate(self.db, sql.format(table), [sid,name,lecture])
        if ret:
            print('Insert data successful ')
        else:
            print('insert failed')

        # 关闭数据库连接
        self.db.close()


    def Insert_lecture(self,table,lec_id, lec_name):
        # inser lecture into database
        sql = "INSERT INTO {0}(lecture_id,lecture_name) VALUES (%s,%s);"
        print(sql.format(table))
        ret = self.executeInsertAndUpdate(self.db, sql.format(table), [lec_id,lec_name])
        if ret:
            print('insert lecture successful')
        else:
            print('insert lecture failed')

        # close db connect
        self.db.close()

    def register(self,table,lec_id, stu_id,lec_num):
        # inser lecture into database
        dt = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        sql = "INSERT INTO {0}(lecture_id,student_id,lecture_num, register_time) VALUES (%s,%s,%s,%s);"
        #print(sql.format(table))
        ret = self.executeInsertAndUpdate(self.db, sql.format(table), [lec_id,stu_id,lec_num,dt])
        if ret:
            print('insert register successful')
        else:
            print('insert register failed')




    def update_register(self,table,lec_id, stu_id,lec_num):
        dt = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        sql = "UPDATE {0} set if_registed=1, register_time=%s where lecture_id=%s and student_id=%s and lecture_num=%s;"
        #print(sql.format(table,lec_id,stu_id,lec_num))
        ret = self.executeInsertAndUpdate(self.db, sql.format(table), [dt, lec_id, stu_id, lec_num])
        if ret:
            print('update register successful')
        else:
            print('update register failed')

        # close db connect


    def QueryID(self,table, lec_name):
        # query who attend the lecture
        sql = "select student_name from {0} where lecture =%s;"
        results = self.executeQuery(self.db, sql.format(table),[lec_name])
        id=[]
        for row in results:
            id.append(row[0])
       # print(id)
       # print(len(id))
       # self.db.close()
        return id,len(id)

    def Query_student_ID(self,table, student_name):
        # query who attend the lecture
        sql = "select id from {0} where student_name =%s;"

        result = self.executeQuery(self.db, sql.format(table),[student_name])
        for row in result:
            id=row[0]
        return id

    def Query_lecture_main_ID(self,table, lecture_id):
        # query who attend the lecture
        sql = "select id from {0} where lecture_id =%s;"
        result = self.executeQuery(self.db, sql.format(table),[lecture_id])
        for row in result:
            id=row[0]
        return id

    def executeQuery(self,con, sql, listParams):
        cursor = con.cursor()  # 使用 execute() 方法执行 SQL 查询
        cursor.execute(sql, listParams)
        results = cursor.fetchall()
        cursor.close()
        return results
    #

    def Query(self,table):
        sql = "select * from {0}"
        results = self.executeQuery(self.db, sql.format(table), [])
        print(results)
        names = []
        sids=[]
        idnum=[]
        # 遍历结果
        for row in results:
            idnum.append(row[0])
            sids.append(row[1])
            names.append(row[2])
        # 关闭数据库连接
        self.db.close()
        return names,sids,idnum


    def check_lecture_situation(self,table, lecture_id,lecture_num):
        # query who attend the lecture
        sql = "select * from {0} where lecture_id =%s and lecture_num=%s;"
        result = self.executeQuery(self.db, sql.format(table),[lecture_id,lecture_num])
        student_id = []
        if_registed=[]
        for row in result:
            student_id.append(row[3])
            if_registed.append(row[5])
        return student_id,if_registed

    def check_student_name(self,table,student_id):
        sql = "select student_name from {0} where id =%s;"
        result = self.executeQuery(self.db, sql.format(table), [student_id])
        for row in result:
            id=row[0]
        return id

    def check_a_student_lectures(self,table,student_id,lecture_id):
        sql = "select * from {0} where student_id =%s and lecture_id=%s;"
        result = self.executeQuery(self.db, sql.format(table), [student_id,lecture_id])
        lecture_num=[]
        registed=[]
        for row in result:
            lecture_num.append(row[2])
            registed.append(row[5])
        return lecture_num,registed