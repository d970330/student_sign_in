# import the necessary packages
from imutils.video import VideoStream
from mysqloper import Mysqloper
import time
import cv2
import os

class face_collect():
    def __init__(self):
        self.operation = Mysqloper()

    def video_demo_collect(self, student_id,student_name,lecture):
        # load OpenCV's Haar cascade for face detection from disk
        detector = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
        # initialize the video stream, allow the camera sensor to warm up,
        # and initialize the total number of example faces written to disk
        # thus far
        print("[INFO] starting video stream...")
        vs = VideoStream(src=0).start()
        # vs = VideoStream(usePiCamera=True).start()
        time.sleep(0.2)
        total = 0

        # loop over the frames from the video stream
        count = 0
        if not os.path.exists('./dataset/%s/' % str(student_name)):
            os.mkdir('./dataset/%s' % str(student_name))
            self.operation.Insert_student('student', student_id, student_name,lecture)
        else:
            print("this name is exist, we are doing a new insert....")
        while True:
            # grab the frame from the threaded video stream, clone it, (just
            # in case we want to write it to disk), and then resize the frame
            # so we can apply face detection faster
            frame = vs.read()
            orig = frame.copy()
           # frame = imutils.resize(frame, width=400)
            # detect faces in the grayscale frame
            rects = detector.detectMultiScale(
                cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY), scaleFactor=1.1,
                minNeighbors=5, minSize=(30, 30))
            # loop over the face detections and draw them on the frame

            for (x, y, w, h) in rects:
                cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)

            # show the output frame
            cv2.imshow("Frame", frame)
            key = cv2.waitKey(1) & 0xFF

            # if the `k` key was pressed, write the *original* frame to disk
            # so we can later process it and use it for face recognition
            if key == ord("k"):
                image = orig[y:y + h, x:x + w]
                p = os.path.sep.join(['./dataset/%s/'% (str(student_name)), "{}.png".format(
                    str(total).zfill(5))])
                cv2.imwrite(p, image)
                total += 1
                print("this is the "+str(total)+" picture, you need 5 pictures totally")
                if total>5:
                    break
            # if the `q` key was pressed, break from the loop
            elif key == ord("q"):
                break
                # print the total faces saved and do a bit of cleanup
        print("[INFO] {} face images stored".format(total))
        print("[INFO] cleaning up...")
        cv2.destroyAllWindows()
        vs.stop()

    def insert_student(self,student_id, student_name, lecture):
        self.operation.Insert_student('student', student_id, student_name, lecture)


    def insert_lecture(self, lecture_id,lecture_name):
        self.operation.Insert_lecture('lecture', lecture_id, lecture_name)

    def register(self, lecture_id,student_id,lecture_num):
        self.operation.register('register', lecture_id, student_id, lecture_num)

    def update(self, lecture_id,student_id,lecture_num):
        self.operation.update_register('register', lecture_id, student_id,lecture_num)

    def query(self, lecture_id):
        self.operation.QueryID('student', lecture_id)

    def student_id(self,student_name):
        student_id =self.operation.Query_student_ID('student',student_name)
        return student_id

    def lecture_main_id(self, lecture_id):
        lecture_id =self.operation.Query_lecture_main_ID('lecture', lecture_id)
        return lecture_id

    def insert_register_for_a_lecture(self,lecture_id, lecture_num):
        name, len = self.operation.QueryID('student', lecture_id)
        for i in range(len):
            self.register(self.lecture_main_id(lecture_id),self.student_id(name[i]),lecture_num)

    def return_value(self,lecture_id):
        name, len = self.operation.QueryID('student', lecture_id)
        for i in range(len):
            print(name[i])

    def return_name(self,student_id):
        name =self.operation.check_student_name('student', student_id)
        return name

    def return_lecture_situation(self,lecture_id,lecture_num):
        student_id,if_registed = self.operation.check_lecture_situation('register',self.lecture_main_id(lecture_id), lecture_num)
        for i in range(len(student_id)):
            name = self.return_name(student_id[i])
            if if_registed[i]==1:
                registed = "registed"
            else:
                registed = "absent"
            print(name+"-----"+registed)

    def return_a_student_situation_of_lectures(self,student_name,lecture_id):
        student_id= self.student_id(student_name)
        lecture_main_id=self.lecture_main_id(lecture_id)
        lecture_num, if_registed= self.operation.check_a_student_lectures('register',student_id,lecture_main_id)
        print("Here is the lecture "+lecture_id+" situation of "+student_name)
        for i in range(len(lecture_num)):
            if if_registed[i] == 1:
                registed = "registed"
            else:
                registed = "absent"
            print(str(lecture_num[i])+"-----"+registed)




