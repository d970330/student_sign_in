from collect import face_collect
from encoding import face_training
from recognition import  Recognition_Face
import sys

class Menu():

    def display_menu(self):
        print("""
Operation Menu:

collect          student_id   student_name   lecture_id      "using camera to collect the picture of faces"
add_student      student_id   student_name   lecture_id      "using code to insert a student information, if you can add the picture directly into dataset"
add_lec          lecture_id   lecture_name                   "adding a lecture to the database"
training                                                     "encoding the face dataset into 128D"
recognition      lecture_id   lecture_num                    "using camera to recognize who attend this lecture"
return           lecture_id   lecture_num                    "using lecturen id and lecture number to check the sign in situation of that lecture"
student          student_name lecture_id                     "return a student register condition"
quit
""")


    def quit(self):
        print("\nThank you for using this script!\n")
        sys.exit(0)

    def run(self):
        collect = face_collect()
        train = face_training()
        recognition = Recognition_Face()
        while True:
            self.display_menu()
            try:
                choice = input("Enter an option: ")
                choice = choice.split()

                if choice[0] == "collect":
                    collect.video_demo_collect(choice[1],choice[2], choice[3])
                elif choice[0] == "add_student":
                    collect.insert_student(choice[1], choice[2], choice[3])
                elif choice[0] == "add_lec":
                    collect.insert_lecture(choice[1], choice[2])
                elif choice[0] == "training":
                    train.training()
                elif choice[0] == "recognition":
                    recognition.recognize(choice[1], choice[2])
                elif choice[0] == "return":
                    collect.return_lecture_situation(choice[1],choice[2])
                elif choice[0] == "student":
                    collect.return_a_student_situation_of_lectures(choice[1],choice[2])
                elif choice[0] == "quit":
                    quit()
                else:
                    input("\npress any key to continue ")


            except Exception as e:
               print("Please input a valid option!"); continue


