/*
 Navicat Premium Data Transfer

 Source Server         : dbs
 Source Server Type    : MySQL
 Source Server Version : 80020
 Source Host           : localhost:3306
 Source Schema         : sa

 Target Server Type    : MySQL
 Target Server Version : 80020
 File Encoding         : 65001

 Date: 12/06/2020 09:43:23
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for lecture
-- ----------------------------
DROP TABLE IF EXISTS `lecture`;
CREATE TABLE `lecture`  (
  `id` int(0) NOT NULL AUTO_INCREMENT,
  `lecture_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `lecture_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `lecture_id`(`lecture_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of lecture
-- ----------------------------
INSERT INTO `lecture` VALUES (1, 'COMP3910', 'alg');
INSERT INTO `lecture` VALUES (2, 'COMP3999', 'logic');
INSERT INTO `lecture` VALUES (3, 'COMP3111', 'bigbangtheory');
INSERT INTO `lecture` VALUES (4, 'COMP4111', 'coding');

SET FOREIGN_KEY_CHECKS = 1;
